## FEATURES ##
### General Features ##
- Compiler Independent
- Non intrusive
- No mandatory parser required
- Variant based interface
- Less possible code duplication

### Supported C++ Constructs ###
- Primitive Types
- Pointer Types
- References
- C Array Types
- Classes
- Multiple Inheritance
- Abstract Classes
- Methods
- Static Functions
- Namespaces
- Class Templates
- Constants
- Enumerators

## ROADMAP ##
- Primitive types casting
- Pointer to reference casting
- General Type casters
- Template value parameter support
- Multiple constructors
- Reflect the standard library
- Parser (?)
